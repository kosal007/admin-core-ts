// BASE URL
const baseUrl = `https://gateway.${process.env.NODE_ENV === 'development' ? 'dev' : 'staging'}.qwiq.io`;

// VERSION
const v = '1';

// BASE ROUTE
const serviceAuthenticate = `${baseUrl}/service-authenticate/${v}`;

export { serviceAuthenticate };
