import React from 'react';
import { LoginFrom } from './components';
import { useAppConsumer } from 'utils/appContext';
import { Redirect } from 'react-router-dom';

const Index: React.FC = () => {
    const { isAuth, login } = useAppConsumer();

    function handleLogin() {
        login();
    }

    if (isAuth) return <Redirect to="/" />;
    return <LoginFrom onFinish={handleLogin} />;
};

export default Index;
