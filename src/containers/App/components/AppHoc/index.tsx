import React, { useState } from 'react';
import { AppCont } from 'utils/appContext';

export interface User {
    id: string;
    fullName: string;
    gender: string;
}
export interface HocProps {
    user?: User | null;
    isAuth: boolean;
}
function wrapComponent(Components: React.FC<HocProps>): React.FC {
    const useChildComponent = () => {
        const [user, setUser] = useState<User | null>(null);
        const [isAuth, setIsAuth] = useState<boolean>(false);

        // Login
        function login(): void {
            setUser({ id: '001', fullName: 'kosal', gender: 'M' });
            setIsAuth(true);
        }

        // Login out
        function logout(): void {
            setUser(null);
            setIsAuth(false);
        }

        // PROPS
        const props = {
            user,
            isAuth,
            login,
            logout,
        };
        return (
            <AppCont.Provider value={{ ...props }}>
                <Components {...props} />
            </AppCont.Provider>
        );
    };
    return useChildComponent;
}

export default wrapComponent;
