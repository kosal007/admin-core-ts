import React, { Suspense } from 'react';
import { useState } from 'react';
import { Switch, Route, useLocation, Redirect } from 'react-router-dom';
import { Layout } from 'antd';
import Loading from 'components/Loading';

import route from '_route';
import nav from '_nav';
import { Header, SideBar } from './components';
import { WrapSider, Logo, LogoText, LogoSubText, WrapContent, CustomLoading } from './style';

const { Footer } = Layout;

const Index: React.FC = () => {
    // STATE
    const [collapsed, setCollapsed] = useState<boolean>(false);
    const { pathname } = useLocation();

    return (
        <Layout>
            <WrapSider collapsed={collapsed} onCollapse={() => setCollapsed(!collapsed)} breakpoint="sm" collapsible>
                <Logo>
                    <LogoText>
                        <LogoSubText>PAGE</LogoSubText> <span>ADMIN</span>
                    </LogoText>
                </Logo>
                <SideBar pathname={pathname} nav={nav} />
            </WrapSider>
            <Layout style={{ marginLeft: collapsed ? 75 : 200 }}>
                <Header />
                <WrapContent>
                    <Suspense
                        fallback={
                            <CustomLoading>
                                <Loading />
                            </CustomLoading>
                        }
                    >
                        <Switch>
                            {route.map((r) => (
                                <Route key={r.id} exact={r.exact} path={r.path} component={r.component} />
                            ))}
                            <Redirect from="/" to="/dashboard" />
                        </Switch>
                    </Suspense>
                </WrapContent>
                <Footer style={{ textAlign: 'center' }}>PAGE ADMIN CORE ©2020 Created by KOSAL</Footer>
            </Layout>
        </Layout>
    );
};
export default Index;
