import styled from 'styled-components';
import { Layout } from 'antd';
const { Content, Sider } = Layout;

const WrapSider = styled(Sider)`
    position: fixed;
    left: 0;
    height: 100vh;
    z-index: 100;
`;

const Logo = styled.div`
    margin: 15px 2px;
`;

const LogoText = styled.h1`
    color: #fff;
    text-align: center;
`;
const LogoSubText = styled.span`
    background-color: #1890ff;
    padding: 2px 15px;
    font-size: 20px;
    border-radius: 5px;
`;

const WrapContent = styled(Content)`
    margin-top: 65px;
    padding: 20px;
    overflow: initial;
    min-height: 80vh;
`;
const CustomLoading = styled.div`
    position: relative !important;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
`;
export { WrapSider, Logo, LogoText, LogoSubText, WrapContent, CustomLoading };
